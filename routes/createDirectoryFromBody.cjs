const express = require("express");
const createDirectory = require("../utils/createDirectory.cjs");
const createAllFiles = require("../utils/createAllFiles.cjs");

const router = express.Router();

router.post("/", (request, response, next) => {
  let nameOfDirectory = request.body.directory;
  let nameOfAllFiles = request.body.files;

  if (Array.isArray(nameOfAllFiles) && typeof nameOfDirectory === "string") {
    createDirectory(nameOfDirectory)
      .then(() => {
        return createAllFiles(nameOfDirectory, nameOfAllFiles);
      })
      .then((dataGot) => {
        if (dataGot.length === nameOfAllFiles.length) {
          response
            .status(200)
            .json({ message: "All Files Created Successfully" });
        } else if (dataGot.length === 0) {
          response.status(400).json({ message: "No File Created" });
        } else {
          response.status(400).json({ message: "Files Created Partially" });
        }
      })
      .catch((errorMessage) => {
        next(errorMessage);
      });
  } else {
    response.status(400).json({
      message:
        "Invalid Data Provided, Please Provide Directory as a String and Filenames inside the Array.",
    });
  }
});

module.exports = router;
