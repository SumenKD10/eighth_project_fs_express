const express = require("express");
const deleteAllFiles = require("../utils/deleteAllFiles.cjs");
const findDirectory = require("../utils/findDirectory.cjs");

const router = express.Router();

router.delete("/", (request, response, next) => {
  let nameOfAllFiles = request.body.files;
  let nameOfDirectory = request.body.directory;

  if (typeof nameOfDirectory === "string" && Array.isArray(nameOfAllFiles)) {
    findDirectory(nameOfDirectory)
      .then((directoryPath) => {
        return deleteAllFiles(directoryPath, nameOfAllFiles);
      })
      .then((dataGotAfterDeletion) => {
        if (dataGotAfterDeletion.length === nameOfAllFiles.length) {
          response
            .status(200)
            .json({ message: "All files are deleted successfully" });
        } else if (dataGotAfterDeletion.length === 0) {
          response.status(400).json({ message: "No File Deleted" });
        } else {
          response.status(400).json({ message: "Only some Files are deleted" });
        }
      })
      .catch((errorMessage) => {
        next(errorMessage);
      });
  } else {
    response.status(400).json({
      message:
        "Invalid data, Please provide directory as a string and name of files inside the Array.",
    });
  }
});

module.exports = router;
