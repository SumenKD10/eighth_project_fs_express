const errorHandler = (errorGot, request, response, next) => {
  if (errorGot.code === "ENOENT") {
    response.status(404).json({ message: "No Such File or Directory" });
  } else if (errorGot.code === "EEXIST") {
    response.status(400).json({
      message: "Directory already exists",
    });
  } else if (errorGot.code === "EACCES") {
    response.status(403).json({
      message: "Permission Denied",
    });
  } else {
    console.log(errorGot);
    if (errorGot.status) {
      response.status(errorGot.status).json(errorGot);
    } else {
      response.status(400).json(errorGot);
    }
  }
};

module.exports = errorHandler;
