const express = require("express");
const logger = require("./middlewares/logger.cjs");
const errorHandler = require("./middlewares/errorHandler.cjs");
const createDirectoryFromBody = require("./routes/createDirectoryFromBody.cjs");
const deleteDirectoryFromBody = require("./routes/deleteDirectoryFromBody.cjs");

function express_fs() {
  //Acquiring express module
  const app = express();

  //Defining Port
  const PORT = process.env.PORT || 8000;

  //Middleware to Log the Host Address on terminal
  app.use(logger);

  //Body Parser
  app.use(express.json());

  //Routes
  app.use("/create", createDirectoryFromBody);
  app.use("/delete", deleteDirectoryFromBody);

  //For All Other Requests
  app.use("*", (request, response) => {
    response.status(400).json({ message: "Invalid Request!" });
  });

  //Error Handler
  app.use(errorHandler);

  //Server Starts Here
  app.listen(PORT, () => {
    console.log(`The server is running on ${PORT} ...`);
  });
}

express_fs();
