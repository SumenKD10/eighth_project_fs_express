const fs = require("fs");
const path = require("path");

function createDirectory(nameOfDirectory) {
  return new Promise((resolve, reject) => {
    if (
      typeof nameOfDirectory !== "string" ||
      nameOfDirectory.includes("/") ||
      nameOfDirectory.trim().length === 0
    ) {
      reject({ message: "Enter a Valid Non-Empty String without /" });
    } else {
      let directoryPath = path.join(__dirname, "..", "public", nameOfDirectory);
      fs.mkdir(directoryPath, (errorGot) => {
        if (errorGot) {
          console.error(errorGot);
          reject(errorGot);
        } else {
          resolve({ message: "Directory got created Successfully." });
        }
      });
    }
  });
}

module.exports = createDirectory;
