const createEachFile = require("./createEachFile.cjs");

function createAllFiles(directoryName, allFileNames) {
  return new Promise((resolve, reject) => {
    let promisesStatusForCreationOfFiles = allFileNames.map((eachFileName) => {
      return createEachFile(directoryName, eachFileName);
    });

    Promise.allSettled(promisesStatusForCreationOfFiles).then((results) => {
      const fulfilledPromises = results.filter((eachPromise) => {
        return eachPromise.status === "fulfilled";
      });
      resolve(fulfilledPromises);
    });
  });
}

module.exports = createAllFiles;
