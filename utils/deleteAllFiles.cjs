const deleteEachFile = require("./deleteEachFile.cjs");

function deleteAllFiles(directoryPath, allFileNames) {
  return new Promise((resolve, reject) => {
    let promisesStatusForCreationOfFiles = allFileNames.map((eachFileName) => {
      return deleteEachFile(directoryPath, eachFileName);
    });

    Promise.allSettled(promisesStatusForCreationOfFiles).then((results) => {
      const fulfilledPromises = results.filter((eachPromise) => {
        return eachPromise.status === "fulfilled";
      });
      resolve(fulfilledPromises);
    });
  });
}

module.exports = deleteAllFiles;
