const fs = require("fs");
const path = require("path");

function findDirectory(directoryName) {
  return new Promise((resolve, reject) => {
    if (directoryName.trim().length === 0 || directoryName.includes("/")) {
      reject({ message: "Enter a Valid Non-Empty String without /" });
    } else {
      let directoryPath = path.join(__dirname, "..", "public", directoryName);
      fs.access(directoryPath, (errorGot) => {
        if (errorGot) {
          console.error(errorGot);
          reject(errorGot);
        } else {
          resolve(directoryPath);
        }
      });
    }
  });
}

module.exports = findDirectory;
