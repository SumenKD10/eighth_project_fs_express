const fs = require("fs");
const path = require("path");
const createError = require("http-errors");

function deleteFile(directoryPath, fileName) {
  return new Promise((resolve, reject) => {
    if (
      typeof fileName !== "string" ||
      fileName.includes("/") ||
      fileName.trim().length === 0 ||
      fileName.includes(".")
    ) {
      reject(
        createError(400, {
          message:
            "File Name Invalid, Please Give file name with non-empty String without / and .",
        })
      );
    } else {
      let filePath = path.join(directoryPath, fileName) + ".json";
      fs.unlink(filePath, (errorGot) => {
        if (errorGot) {
          reject(errorGot);
        } else {
          resolve("File Deleted Successfully");
        }
      });
    }
  });
}

module.exports = deleteFile;
