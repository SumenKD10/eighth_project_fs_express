const fs = require("fs");
const path = require("path");
const createError = require("http-errors");

function createFiles(directoryName, fileName) {
  return new Promise((resolve, reject) => {
    if (
      typeof fileName !== "string" ||
      fileName.includes("/") ||
      fileName.trim().length === 0 ||
      fileName.includes(".")
    ) {
      reject(
        createError(400, {
          message:
            "Incorrect File Name, Enter Filename as Non-Empty string without / or .",
        })
      );
    } else {
      let filePath = path.join(
        __dirname,
        "..",
        "public",
        directoryName,
        fileName + ".json"
      );
      let dataToEnter = JSON.stringify({ data: "This is File Data" });
      fs.writeFile(filePath, dataToEnter, (errorGot) => {
        if (errorGot) {
          console.error(errorGot);
          reject(errorGot);
        } else {
          resolve("File Created Successfully");
        }
      });
    }
  });
}

module.exports = createFiles;
